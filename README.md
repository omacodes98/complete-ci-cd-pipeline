# Complete CI/CD Pipeline 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

* CI step: Increment version

* CI step: Built artifact for Java Maven application

* CI step: Built and push Docker image to Docker Hub

* CD step: Deployed new application version with Docker Compose

* CD step: Commit the version update

## Technologies Used

* AWS

* Jenkins 

* Docker

* Linux 

* Git 

* Java 

* Maven 

* Docker Hub 

## Steps 

Step 1: Create a jenkinsfile with the following stages increment version, build app, build image, Deploy and commit 

note: I built a couple projects focused on explaining the CI stages step by step. If you want some more information the project repos are down below 

CI CD wih dockercompose: https://gitlab.com/omacodes98/cd-with-jenkins-to-ec2-instances-with-docker-compose.git

CI pipeline with jenkins: https://gitlab.com/omacodes98/creating-ci-pipeline-with-jenkins.git

CI incrementing version: https://gitlab.com/omacodes98/incrementing-application-version-in-jenkins.git

CI Jenkins installation: https://gitlab.com/omacodes98/installing-jenkins-on-digitalocean-cloud-server.git

CI CD nodejs project: https://gitlab.com/omacodes98/nodejs-complete-ci-cd-pipeline.git

Step 2: Add versioning stage 

[Stage versioning](/images/01_adding_versioning_stage.png)

Step 3: Add a deploy stage and make sure you add necessary environmental variable for dynamic versioning 

[Build Stage and Environmental variables](/images/02_inserting_needed_environmental_variables_in_build_and_deploy_stage.png)

Stage 4: Add the last stage which is jenkins commit to prevent commit loop 

[Jenkins Commit stage](/images/03_inserting_jenkins_commit_to_enable_versioning.png)

Stage 5: Push changes to repo and check  multibranch pipeline job for success

[Successful logs](/images/04_successful_logs.png)
[Jenkins commit](/images/05_version_bump.png)

Note: You will need to install ignore committer strategy plugin for this as you would need to put jenkins email to ignore it from triggering auto jobs 
## Installation

Run $ sudo apt install maven

## Usage 

Run $ java -jar java-maven-app*.jar

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/complete-ci-cd-pipeline.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using mvn test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/complete-ci-cd-pipeline

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.